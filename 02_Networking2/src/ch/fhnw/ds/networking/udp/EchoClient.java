package ch.fhnw.ds.networking.udp;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;

class EchoClient {

	public static void main(String args[]) throws Exception {
		String host = "localhost";
		InetAddress ia = InetAddress.getByName(host);

		try (DatagramSocket socket = new DatagramSocket()) {
			DatagramPacket packet = new DatagramPacket(new byte[512], 512, ia, 1234);
			socket.setSoTimeout(1000);

			while (true) {
				socket.send(packet);
				try {
					socket.receive(packet);
					System.out.println(new String(packet.getData(), packet.getOffset(), packet.getLength()));
					System.out.println(packet.getSocketAddress());
					Thread.sleep(1000);
				} catch (SocketTimeoutException e) {
					System.out.println("no answer received");
				}
			}
		}
	}

}
