package ch.fhnw.ds.networking.udp;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.Date;

public class EchoServer {
	
	public static void main(String args[]) throws Exception {
		try ( DatagramSocket socket = new DatagramSocket(1234)) {
			System.out.println(socket.getLocalAddress());
			System.out.println(socket.getLocalPort());
			DatagramPacket packet = new DatagramPacket(new byte[1024], 1024);
	
			while (true) {
				socket.receive(packet);
				byte[] data = new Date().toString().getBytes();
				packet.setData(data);
				packet.setLength(data.length);
				socket.send(packet);
			}
		}
	}

}
