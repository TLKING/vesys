package ch.fhnw.ds.networking.udp.multicast;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class Receiver {
	private static final int BUFSIZE = 508;

	public static void main(String[] args) throws Exception {
		String multicastAddress = "228.5.6.7";
		int port = 50000;
		
		try (MulticastSocket socket = new MulticastSocket(port)) {
//			socket.setTimeToLive(1);
			System.out.println(socket.getTimeToLive());
			socket.joinGroup(InetAddress.getByName(multicastAddress));
			DatagramPacket packetIn = new DatagramPacket(new byte[BUFSIZE], BUFSIZE);
			while (true) {
				socket.receive(packetIn);
				String received = new String(packetIn.getData(), 0, packetIn.getLength());
				System.out.println(received);
			}
		}
	}
}