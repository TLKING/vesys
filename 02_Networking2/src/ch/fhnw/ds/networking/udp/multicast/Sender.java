package ch.fhnw.ds.networking.udp.multicast;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Random;

public class Sender {
	private static final int BUFSIZE = 508;
	private static final int LOOPS = 10;

	public static void main(String[] args) throws Exception {
		String host = "228.5.6.7";
		int port = 50000;

		try (DatagramSocket socket = new DatagramSocket()) {
			InetAddress addr = InetAddress.getByName(host);
			DatagramPacket packet = new DatagramPacket(new byte[BUFSIZE], BUFSIZE, addr, port);
			Random random = new Random();
			for (int i = 0; i < LOOPS; i++) {
				int value = random.nextInt();
				byte[] data = String.valueOf(value).getBytes();
				packet.setData(data);
				packet.setLength(data.length);
				socket.send(packet);
				Thread.sleep(3000);
			}
		}
	}
}