package ch.fhnw.ds.internet.htmlunit;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlNumberInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;

public class Client {

	public static void main(String[] args) throws Exception {
		try (WebClient webClient = new WebClient()) {
			HtmlPage page = webClient.getPage("http://localhost:80/register");

			HtmlForm form = page.getFormByName("register");

			HtmlTextInput userField = form.getInputByName("user");
			userField.setText("Dominik" + System.currentTimeMillis());
			HtmlNumberInput amountField = form.getInputByName("amount");
			amountField.setText("123");

			HtmlSubmitInput button = form.getInputByName("submit");
			button.click();
		}
	}

}
