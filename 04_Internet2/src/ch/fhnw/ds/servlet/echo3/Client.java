package ch.fhnw.ds.servlet.echo3;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class Client {

	public static void main(String[] args) throws Exception {
		URL url = new URL("http://localhost:8080/echo2/obj");
		HttpURLConnection c = (HttpURLConnection) url.openConnection();
		c.setDoOutput(true);
		// c.setRequestMethod("POST");
		// c.setRequestProperty("User-Agent", "Mozilla/5.0");

		ObjectOutputStream oos = new ObjectOutputStream(c.getOutputStream());

		oos.writeObject("Hello");
		oos.flush();
		// oos.close();

		int responseCode = c.getResponseCode();
		System.out.println("Response Code: " + responseCode);

		ObjectInputStream ois = new ObjectInputStream(c.getInputStream());
		Object res = ois.readObject();
		System.out.println(res);
	}

}
