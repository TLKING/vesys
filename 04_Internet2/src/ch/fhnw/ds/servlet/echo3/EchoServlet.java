/*
 * Copyright (c) 2000-2016 Fachhochschule Nordwestschweiz (FHNW)
 * All Rights Reserved. 
 */

package ch.fhnw.ds.servlet.echo3;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/obj")
public class EchoServlet extends HttpServlet {
	// This servlet is packed into echo2.jar, so it is accessible under the url /echo2/obj
	
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		ObjectOutputStream out = new ObjectOutputStream(response.getOutputStream());
		ObjectInputStream in = new ObjectInputStream(request.getInputStream());
		
		try {
			Object x = in.readObject();
			System.out.println("do Post methode: " + x);
			out.writeObject(x);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
		
}
