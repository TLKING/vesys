package ch.fhnw.ds.hello.client;

import ch.fhnw.ds.hello.client.jaxws.HelloServiceImpl;
import ch.fhnw.ds.hello.client.jaxws.HelloServiceImplService;

public class Client {

	public static void main(String[] args) {
		HelloServiceImplService service = new HelloServiceImplService();
		HelloServiceImpl port = service.getHelloServiceImplPort();
		
		String result = port.sayHello("Dominik");
		System.out.println(result);
	}
}

