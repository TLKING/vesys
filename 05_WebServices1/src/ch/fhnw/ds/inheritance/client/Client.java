package ch.fhnw.ds.inheritance.client;

import ch.fhnw.ds.inheritance.client.jaxws.A;
import ch.fhnw.ds.inheritance.client.jaxws.B;
import ch.fhnw.ds.inheritance.client.jaxws.Echo;
import ch.fhnw.ds.inheritance.client.jaxws.EchoService;

public class Client {

	public static void main(String[] args) {
		EchoService service = new EchoService();
		Echo port = service.getEchoPort();
		
		A a = new A();
		a.setValue(11);
		a = (A)port.echo(a);
		System.out.println(a.getValue());
		
		B b = new B();
		b.setName("Voser");
		b = (B)port.echo(b);
		System.out.println(b.getName());
	}

}
