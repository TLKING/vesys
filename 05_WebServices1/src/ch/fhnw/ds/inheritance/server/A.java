package ch.fhnw.ds.inheritance.server;

import javax.xml.bind.annotation.XmlElement;

public class A extends Request {
	@XmlElement
	private int value;

	@Override
	public void execute() {
		value++;
	}

}
