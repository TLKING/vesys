package ch.fhnw.ds.inheritance.server;

import javax.xml.bind.annotation.XmlElement;

public class B extends Request {
	@XmlElement
	private String name;

	@Override
	public void execute() {
		name = "Hello " + name;
	}

}
