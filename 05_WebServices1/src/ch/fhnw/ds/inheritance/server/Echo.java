package ch.fhnw.ds.inheritance.server;

import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;

@WebService
@XmlSeeAlso({A.class, B.class})
public class Echo {

	public Request echo(Request request) {
		request.execute();
		return request;
	}
}
