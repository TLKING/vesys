package ch.fhnw.ds.inheritance.server;

public abstract class Request {
	public abstract void execute();
}
