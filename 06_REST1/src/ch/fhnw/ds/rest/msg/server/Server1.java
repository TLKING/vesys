package ch.fhnw.ds.rest.msg.server;

import java.io.IOException;
import java.net.URI;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

public class Server1 {

	public static void main(String[] args) throws IOException {

		final String baseUri = "http://localhost:9998/";

        // create a resource config that scans for JAX-RS resources and providers in ch.fhnw.ds.rest.msg.resources package
		// @Singleton annotations will be respected 
	    final ResourceConfig rc = new ResourceConfig().packages("ch.fhnw.ds.rest.msg.resources");
		
		System.out.println("Starting grizzly...");
		HttpServer httpServer = GrizzlyHttpServerFactory.createHttpServer(URI.create(baseUri), rc);
		
		System.out.println(String.format("Jersey app started with WADL available at "
								+ "%sapplication.wadl\nTry out %smsg\nHit enter to stop it...",
								baseUri, baseUri));

		System.in.read();
		httpServer.shutdown();
	}
	
}

