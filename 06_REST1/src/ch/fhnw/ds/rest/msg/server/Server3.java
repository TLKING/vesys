package ch.fhnw.ds.rest.msg.server;

import java.io.IOException;
import java.net.URI;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;


//import com.sun.jersey.api.container.grizzly2.GrizzlyServerFactory;
//import com.sun.jersey.api.core.ApplicationAdapter;
//import com.sun.jersey.api.core.ResourceConfig;

public class Server3 {

	public static void main(String[] args) throws IOException {

		final String baseUri = "http://localhost:9998/";

		// @Singleton annotation is respected,
		// but classes without a @Singleton annotation can also returned as singletons over the application definition
		final ResourceConfig rc = ResourceConfig.forApplication(new MsgApplication());
		
		System.out.println("Starting grizzly...");
		HttpServer httpServer = GrizzlyHttpServerFactory.createHttpServer(URI.create(baseUri), rc);

		System.out.println(String.format("Jersey app started with WADL available at "
								+ "%sapplication.wadl\nTry out %smsg\nHit enter to stop it...",
								baseUri, baseUri));

		System.in.read();
		httpServer.shutdown();
	}
	
}
