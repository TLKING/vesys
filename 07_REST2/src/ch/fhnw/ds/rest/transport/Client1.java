package ch.fhnw.ds.rest.transport;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import ch.fhnw.transport.Location;
import ch.fhnw.transport.Stations;

public class Client1 {
	public static void main(String[] args) {
		Client c = ClientBuilder.newClient();
		WebTarget r = c.target("http://transport.opendata.ch/v1/locations?x=47.48&y=8.21");

		// Access all nearby Stations around the given coordinates and print
		// their names
		Stations res = r.request().accept("application/json").get(Stations.class);
		
		for (Location l : res.getStations())
			System.out.println(l.getId() + "\t" + l.getName());
	}
}
