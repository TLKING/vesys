package ch.fhnw.ds.spark;

import static spark.Spark.get;

import spark.Spark;


public class Test {
    public static void main(String[] args) {
    	get("/hello", "text/plain", (req, res) -> "Hello World1");
    	get("/hello", "text/html", (req, res) -> "<h1>Hello World2</h2>");
        get("/hello", (req, res) -> req.pathInfo());
        get("/hello/:name", (req, res) -> "Hello World "+req.params(":name"));
        get("say/*/to/*", (req, res) -> "number of splat params: " + req.splat().length);
        get("/redirect", (req,res) -> {res.redirect("/hello");return null;});
        
        Spark.init();	// default port: 4567
    }
}
