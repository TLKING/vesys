package ch.fhnw.ds.rest.resources;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.inject.Singleton;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@Singleton
@Path("polls")
@Api(value = "/doodle")
public class DoodleResource {
	private Map<String, Poll> polls = new HashMap<>();
	
	@GET
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get all polls", notes = "Returns a list of all registered polls.")
    @ApiResponses(value = {
		  @ApiResponse(code = 200, message = "List of current polls"),
          @ApiResponse(code = 500, message = "Something wrong in Server")})
	public Response getPolls() {
		System.out.println("getPolls => " + polls.keySet().toString());
		
		JsonArrayBuilder arr = Json.createArrayBuilder();
		for(String s : polls.keySet()) arr.add(s);
        JsonObject value = Json.createObjectBuilder()
                .add("polls", arr.build())
                .build();
        return Response.status(200).entity(value).build();
	}
	
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Create a new poll", notes = "Create a new poll and provide the options.")
	@ApiResponses(value = {
		@ApiResponse(code = 201, message = "poll created"
			// ,responseHeaders = @ResponseHeader(name = "Location", description = "specifies the location of the new poll", response = String.class)
		), 
		@ApiResponse(code = 500, message = "Something wrong in Server") 
	})
	public Response createPoll(@Context UriInfo uriInfo, Poll p) {
		String uuid = UUID.randomUUID().toString();
		System.out.println("created poll " + uuid);
		p.setClosed(false);
		polls.put(uuid, p);
		URI location = uriInfo.getAbsolutePathBuilder().path(uuid).build();
		return Response.created(location).build();
	}

	
    @PUT
	@Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Update poll", notes = "Updates the poll object.")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK, update successful"),
        @ApiResponse(code = 404, message = "Poll does not exist")})
    public Response updatePoll(@PathParam("id") String id, Poll p2) {
    	System.out.println("getPoll " + id);
        Poll p = polls.get(id);
        if(p == null) return Response.status(404).build();
        polls.put(id, p2);
        return Response.status(200).entity(p2).build();
    }

	
    @GET
	@Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get poll", notes = "Returns the poll object.")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 404, message = "Poll does not exist")})
    public Response getPoll(@PathParam("id") String id) {
    	System.out.println("getPoll " + id);
        Poll p = polls.get(id);
        if(p == null) return Response.status(404).build();
        return Response.status(200).entity(p).build();
    }

    
    @DELETE
	@Path("{id}")
    @ApiOperation(value = "Delete poll", notes = "deletes a closed poll")
    @ApiResponses(value = {
        @ApiResponse(code = 404, message = "Poll does not exist or has already been deleted"),
        @ApiResponse(code = 409, message = "Poll is still open, needs to be closed first."),
        @ApiResponse(code = 204, message = "Poll has been deleted successfully")})
    public Response deletePoll(@PathParam("id") String id) {
        Poll p = polls.get(id);
        if(p == null) {
        	return Response.status(404).build();
        } else if(!p.isClosed()) {
        	return Response.status(409).build();
        }
        polls.remove(id);
        return Response.noContent().build();
    }


}