package ch.fhnw.ds.rest.resources;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Path("hello")
@Api(value = "/hello")
public class HelloResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Say Hello World", notes = "Anything Else?")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    public Response sayHello() {
        JsonObject value = Json.createObjectBuilder()
                .add("firstName", "Jeremy")
                .add("lastName", "Chung")
                .add("message", "Hello World!")
                .build();
        return Response.status(200).entity(value).build();
    }


    @GET
    @Path("msg")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @ApiOperation(value = "query message",
            notes = "Returns an instance of the message object")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    public Msg sayHelloText(/*@HeaderParam("Accept") String type*/) {
    	System.out.println("GET msg called");
        return new Msg("Hello World!");
    }

    @PUT
    @Path("msg")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
//    @Consumes({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "set new message",
            notes = "Sets a new message")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    @Deprecated
    public void setNewText(/*@HeaderParam("Content-Type") String type, */Msg msg) {
    	System.out.println("setNewText called");
  //  	System.out.println(type);
    	System.out.println(msg.getText());
    }
}