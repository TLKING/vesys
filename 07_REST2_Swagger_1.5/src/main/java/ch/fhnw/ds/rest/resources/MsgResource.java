package ch.fhnw.ds.rest.resources;

import java.net.URI;

import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import io.swagger.annotations.Api;
import io.swagger.annotations.Info;
import io.swagger.annotations.SwaggerDefinition;

@Singleton
@Path("msg")
@Api(value = "/msg")
@SwaggerDefinition(
        info = @Info(
                description = "Operations to get and set a single message",
                version = "V1.0.2017",
                title = "The Message API"
        )
)
public class MsgResource {
	private String msg = "Hello, world!";

	public MsgResource() {
		System.out.println("MsgResource() called");
	}

	// GET on /msg
	// ===========

//	@GET
//	@Produces("text/plain")
//	public String getPlain() {
//		return msg + "\n";
//	}
//
//	@GET
//	@Produces("text/html")
//	public String getHtml() {
//		StringBuffer buf = new StringBuffer();
//		buf.append("<html><body><h1>Message Text</h1>" + msg + "<br>");
//		buf.append("<form method=\"POST\" action=\"/msg\">");
//		buf.append("<p>Text: <input name=\"msg\" type=\"text\" size=20/>");
//		buf.append("<input type=\"submit\" value=\"Submit\" />");
//		buf.append("</form>");
//		buf.append("</body></html>");
//		return buf.toString();
//	}
//
//	@GET
//	@Produces("text/xml")
//	public String getSimpleXml() {
//		return "<string>" + msg + "</string>";
//	}

	@GET
	@Produces( { "application/json", "application/xml" })
	public Msg getXml() {
		return new Msg(msg);
	}

	// PUT on /msg
	// ===========

//	@PUT
//	@Consumes("text/plain")
//	public void setTextPlain(String new_msg) {
//		msg = new_msg;
//	}

	@PUT
	@Consumes( { "application/json", "application/xml" })
	public void setTextXml(Msg message) {
		msg = message.getText();
	}

//	@PUT
//	@Consumes("application/x-www-form-urlencoded")
//	@Produces("text/xml")
//	public String setTextForm(@FormParam("msg") String new_msg) {
//		msg = new_msg;
//
//		ByteArrayOutputStream stream = new ByteArrayOutputStream();
//		XMLEncoder enc = new XMLEncoder(stream);
//		enc.writeObject(new_msg);
//		enc.close();
//		return new String(stream.toByteArray()) + "\n";
//	}

	// POST on /msg (used for forms)
	// =============================

//	@POST
//	@Consumes("application/x-www-form-urlencoded")
//	@Produces("text/html")
//	public String doPost(@FormParam("msg") String new_msg) {
//		msg = new_msg;
//		return getHtml();
//	}

	@POST
	@Consumes( { "application/xml", "application/json" })
	public Response createNewMessage(@Context UriInfo uriInfo, Msg message) {
		URI location = uriInfo.getAbsolutePathBuilder().path(message.getText())
				.build();
		return Response.created(location).build();
	}

	// DELETE on /msg
	// ==============

	@DELETE
	@Produces("text/plain")
	public String onDelete() {
		msg = null;
		return "Message deleted.\n";
	}

	// GET on /msg/cc
	// ==============

	@GET
	@Path("cc")
	@Produces("text/plain")
	public Response getPlain2() {
		ResponseBuilder builder = Response.ok(msg + "\n");
		CacheControl cc = new CacheControl();
		cc.setMaxAge(1000); // HTTP max-age field, in seconds
		cc.setNoTransform(true);
		cc.setPrivate(true);
		builder.cacheControl(cc);
		return builder.build();
	}

	// GET on /msg/{id} and on /msg/{id}/headers
	// =========================================

	@GET
	@Produces("text/plain")
	@Path("{id}")
	public String readDetailsInfo(@PathParam("id") String path) {
		return msg + ": " + path + "\n";
	}

	@GET
	@Produces("text/plain")
	@Path("{id}/headers")
	public String readDetailHeaders(@PathParam("id") String path,
			@Context HttpHeaders headers) {
		StringBuffer buf = new StringBuffer();
		buf.append("Headers of request " + path + "\n\n");
		MultivaluedMap<String, String> map = headers.getRequestHeaders();
		for (String key : map.keySet()) {
			buf.append(key + ": " + map.getFirst(key) + "\n");
		}
		return buf.toString();
	}

}
