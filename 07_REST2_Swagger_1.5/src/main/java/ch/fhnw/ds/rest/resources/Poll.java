package ch.fhnw.ds.rest.resources;

import javax.xml.bind.annotation.XmlRootElement;

import io.swagger.annotations.ApiModel;

@XmlRootElement
@ApiModel(value = "A poll represents a doodle poll. It contains the name of the owner and the possible options.")
public class Poll {
	private String owner;
	private String[] options;
	boolean closed;
	
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String[] getOptions() {
		return options;
	}
	public void setOptions(String[] options) {
		this.options = options;
	}
	public boolean isClosed() {
		return closed;
	}
	public void setClosed(boolean closed) {
		this.closed = closed;
	}

}
