package ch.fhnw.ds.rest.servers;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import io.swagger.jaxrs.config.BeanConfig;

// https://github.com/swagger-api/swagger-core/wiki/Swagger-Core-JAX-RS-Project-Setup-1.5.X
// https://github.com/swagger-api/swagger-core/wiki/1.3--1.5-Migration

@ApplicationPath("/")
public class MsgApplication extends Application {
	private Set<Object> singletons = new HashSet<Object>();
	private Set<Class<?>> classes = new HashSet<Class<?>>();

	public MsgApplication() {
//		singletons.add(new MsgResource());
		// classes.add(HelloResource.class);
		// classes.add(MsgResource.class);
		
        classes.add(io.swagger.jaxrs.listing.ApiListingResource.class);
        classes.add(io.swagger.jaxrs.listing.SwaggerSerializers.class);

		
        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setVersion("1.0.2017");
        beanConfig.setDescription("Services discussed in the vesys module");
        beanConfig.setTitle("Sample Services");
        
        // configuration where the service runs (used to generate the links)
        beanConfig.setSchemes(new String[]{"http"});
        beanConfig.setHost("localhost:8080");
        beanConfig.setBasePath("/");
        
        beanConfig.setScan(true);
        beanConfig.setResourcePackage("ch.fhnw.ds.rest.resources");
        
        beanConfig.setPrettyPrint(true);
	}

	@Override
	public Set<Class<?>> getClasses() {
		return classes;
	}

	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}
}

