package ch.fhnw.ds.rest.servers;
import java.io.IOException;
import java.net.URI;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import ch.fhnw.ds.rest.resources.DoodleResource;
import io.swagger.jaxrs.config.BeanConfig;

public class Server {

    private static final URI BASE_URI = URI.create("http://localhost:8080/");

    public static void main(String[] args) throws IOException {
            BeanConfig beanConfig = new BeanConfig();
            // version displayed together with base url on overview page (for all provided resources)
            beanConfig.setVersion("1.0.2017");
            beanConfig.setDescription("Services discussed in the vesys module");
            beanConfig.setTitle("Sample Services");
            
            beanConfig.setScan(true);
            beanConfig.setResourcePackage(DoodleResource.class.getPackage().getName());
            
            // used to define the URLs to be used by swagger to access the resource
            beanConfig.setSchemes(new String[]{"http"});
            beanConfig.setHost("localhost:8080");
            beanConfig.setBasePath("");

            beanConfig.setPrettyPrint(true);
            
            final HttpServer server = GrizzlyHttpServerFactory.createHttpServer(BASE_URI, createApp());

            System.out.println("Application started.\nHit enter to stop it...");
            
            System.in.read();
            server.shutdownNow();
            System.exit(0);
    }

    public static ResourceConfig createApp() {
        return new ResourceConfig().
                packages(
                	DoodleResource.class.getPackage().getName(),
                    "io.swagger.jaxrs.listing"
                ).
                //register(createMoxyJsonResolver()).
                register(CORSFilter.class);
    }

//    public static ContextResolver<MoxyJsonConfig> createMoxyJsonResolver() {
//        final MoxyJsonConfig moxyJsonConfig = new MoxyJsonConfig();
//        Map<String, String> namespacePrefixMapper
//                = new HashMap<String, String>(1);
//        namespacePrefixMapper.put(
//                "http://www.w3.org/2001/XMLSchema-instance", "xsi");
//        moxyJsonConfig.setNamespacePrefixMapper(namespacePrefixMapper)
//                .setNamespaceSeparator(':');
//        return moxyJsonConfig.resolver();
//    }

    static class CORSFilter implements ContainerResponseFilter {
        @Override
        public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
                throws IOException {
        	responseContext.getHeaders().add("Access-Control-Allow-Origin", "*");
        	responseContext.getHeaders().add("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
        	responseContext.getHeaders().add("Access-Control-Allow-Headers", "Content-Type, Accept");
        }       
    }
}