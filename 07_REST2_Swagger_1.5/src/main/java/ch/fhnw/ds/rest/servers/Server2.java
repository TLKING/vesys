package ch.fhnw.ds.rest.servers;

import java.io.IOException;
import java.net.URI;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;


//http://localhost:8080/swagger.json

public class Server2 {

	public static void main(String[] args) throws IOException {

		final String baseUri = "http://localhost:8080/";

		final ResourceConfig rc = ResourceConfig.forApplication(new MsgApplication()).register(CORSFilter.class);
		
		System.out.println("Starting grizzly at " + baseUri);
		HttpServer httpServer = GrizzlyHttpServerFactory.createHttpServer(URI.create(baseUri), rc);

		System.in.read();
		httpServer.shutdown();
	}
	
    static class CORSFilter implements ContainerResponseFilter {
        @Override
        public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
                throws IOException {
        	responseContext.getHeaders().add("Access-Control-Allow-Origin", "*");
        	responseContext.getHeaders().add("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
        	responseContext.getHeaders().add("Access-Control-Allow-Headers", "Content-Type, Accept");
        }       
    }

	
}
