package ch.fhnw.ds.rest.servers;

// In order to run this server, additional dependencies have to be added to the project (=> pom.xml).
import java.net.URI;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.servlet.ServletRegistration;
import org.glassfish.grizzly.servlet.WebappContext;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;

import io.swagger.jaxrs.config.BeanConfig;

public class Server3 {
	public static final String BASE_URI = "http://localhost:8080";

	public static void main(String[] args) throws Exception {
		BeanConfig beanConfig = new BeanConfig();
		beanConfig.setVersion("1.0.2");
		beanConfig.setBasePath("/");
		beanConfig.setResourcePackage("ch.fhnw.ds.rest.msg");
		beanConfig.setScan(true);

		HttpServer httpServer = GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), new ResourceConfig());

		// Initialize and register Jersey Servlet
		WebappContext context = new WebappContext("WebappContext", "");
		ServletRegistration registration = context.addServlet("ServletContainer", ServletContainer.class);
		registration.setInitParameter("javax.ws.rs.Application", MsgApplication.class.getName());
		registration.addMapping("/*");

//		WebappContext context = new WebappContext("Currency Converter", "/currency");
//		ServletRegistration registration = context.addServlet("Converter", Converter.class);
//		registration.addMapping("/*");

		context.deploy(httpServer);
	}
}