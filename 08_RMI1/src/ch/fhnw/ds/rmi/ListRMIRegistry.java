package ch.fhnw.ds.rmi;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

public class ListRMIRegistry {

	public static void main(String[] args) throws Exception {
		try {
			LocateRegistry.createRegistry(1099);
		} catch (RemoteException e) {
			System.out.println(">> registry could not be exported");
			System.out.println(">> probably another registry already runs on 1099");
		}

		String host = "localhost";
		int port = 1099;
		if (args.length > 0) {
			host = args[0];
			if (args.length > 1) {
				port = Integer.parseInt(args[1]);
			}
		}

		String s[] = Naming.list("rmi://" + host + ":" + port);
		for (int i = 0; i < s.length; i++) {
			System.out.println(s[i]);
		}
	}

}
