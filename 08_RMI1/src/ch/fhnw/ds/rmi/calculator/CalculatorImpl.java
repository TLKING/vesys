package ch.fhnw.ds.rmi.calculator;

import java.rmi.server.UnicastRemoteObject;

public class CalculatorImpl extends UnicastRemoteObject implements Calculator {

	public CalculatorImpl() throws java.rmi.RemoteException {
		super();
	}

	public CalculatorImpl(int port) throws java.rmi.RemoteException {
		super(port);
	}

	@Override
	public long add(long a, long b) {
		return a + b;
	}

	@Override
	public long sub(long a, long b) {
		return a - b;
	}

	@Override
	public long mul(long a, long b) {
		return a * b;
	}

	@Override
	public long div(long a, long b) {
		return a / b;
	}

}
