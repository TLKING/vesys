package ch.fhnw.ds.rmi.calculator;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class CalculatorImpl extends UnicastRemoteObject implements Calculator {

	public CalculatorImpl() throws RemoteException {
	}

	public CalculatorImpl(int port) throws RemoteException {
		super(port);
	}

	public long add(long a, long b) {
		return a + b;
	}

	public long sub(long a, long b) {
		return a - b;
	}

	public long mul(long a, long b) {
		return a * b;
	}

	public long div(long a, long b) {
		return a / b;
	}
}
