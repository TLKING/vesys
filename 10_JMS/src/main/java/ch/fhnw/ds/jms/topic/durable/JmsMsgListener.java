package ch.fhnw.ds.jms.topic.durable;

import javax.jms.ConnectionFactory;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Topic;
import javax.naming.Context;
import javax.naming.InitialContext;

public class JmsMsgListener {

	public static void main(String[] args) throws Exception {
		Context jndiContext = new InitialContext();

		ConnectionFactory factory = (ConnectionFactory) jndiContext.lookup("ConnectionFactory");
		Topic topic = (Topic) jndiContext.lookup("/topic/APPL");

		System.out.println("topic.getTopicName() = " + topic.getTopicName());
		System.out.println("topic.toString() = " + topic.toString());

		try (JMSContext context = factory.createContext()) {
			context.setClientID("Gruntz");
			JMSConsumer subscriber1 = context.createDurableConsumer(topic, "Subscription1");
			subscriber1.setMessageListener((Message msg) -> {
				try {
					System.out.println("Subscription1: " + msg.getBody(String.class));
				} catch (JMSException e) {
				}
			});

//			JMSConsumer subscriber2 = context.createDurableConsumer(topic, "Subscription2");
//			subscriber2.setMessageListener((Message msg) -> {
//				try {
//					System.out.println("Subscription2: " + msg.getBody(String.class));
//				} catch (JMSException e) {
//				}
//			});

			System.in.read();
		}
	}

}