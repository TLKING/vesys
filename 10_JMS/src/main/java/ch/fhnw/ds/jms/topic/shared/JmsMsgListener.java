package ch.fhnw.ds.jms.topic.shared;

import javax.jms.ConnectionFactory;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.Topic;
import javax.naming.Context;
import javax.naming.InitialContext;

public class JmsMsgListener {

	public static void main(String[] args) throws Exception {
		Context jndiContext = new InitialContext();

		ConnectionFactory factory = (ConnectionFactory) jndiContext.lookup("ConnectionFactory");
		Topic topic = (Topic) jndiContext.lookup("/topic/APPL");

		try (JMSContext context = factory.createContext()) {
			// JMSConsumer subscriber = context.createConsumer(topic);
			JMSConsumer subscriber = context.createSharedConsumer(topic, "SharedSubscription");
			subscriber.setMessageListener(msg -> {
				try {
					System.out.println("SharedSubscription: " + msg.getBody(String.class));
				} catch (JMSException e) {
				}
			});

			Object x = new Object();
			synchronized (x) {
				x.wait();
			}

		}
	}

}