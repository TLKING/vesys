package ch.fhnw.ds.jms.transactions;

import javax.jms.ConnectionFactory;
import javax.naming.Context;
import javax.naming.InitialContext;

public class Demo {
	
	public static void main(String[] args) throws Exception {
		Context jndiContext = new InitialContext();
		ConnectionFactory factory = (ConnectionFactory) jndiContext.lookup("ConnectionFactory");

		Retailer r = new Retailer("VendorOrderQueue", factory);
		Vendor v = new Vendor("VendorOrderQueue", "MonitorOrderQueue", "StorageOrderQueue", factory);
		Supplier s1 = new Supplier("HardDrive", "StorageOrderQueue", factory);
		Supplier s2 = new Supplier("Monitor", "MonitorOrderQueue", factory);
		
		new Thread(r, "Retailer").start();
		new Thread(v, "Vendor").start();
		new Thread(s1, "Supplier 1").start();
		new Thread(s2, "Supplier 2").start();
	}

}