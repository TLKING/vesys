package ch.fhnw.ds.jms.transactions;

import java.util.UUID;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.TemporaryQueue;
import javax.naming.Context;
import javax.naming.InitialContext;

/**
 * The Retailer orders computers from the Vendor by sending a message via the VendorOrderQueue. 
 * It then synchronously receives the response message and reports if the order was successful 
 * or not.
 */
public class Retailer implements Runnable {
	private static final int NOFORDERS = 5;
	private final String queue;
	private final ConnectionFactory factory;

	public Retailer(String queue, ConnectionFactory factory) throws JMSException {
		this.queue = queue;
		this.factory = factory;
	}

	public void run() {
		try (JMSContext context = factory.createContext()) {
			// The Retailer's context is non-trasacted, i.e. JMSContext.AUTO_ACKNOWLEDGE
			Destination vendorOrderQueue = context.createQueue(queue);
			TemporaryQueue retailerConfirmQueue = context.createTemporaryQueue();

			JMSProducer producer = context.createProducer().setJMSReplyTo(retailerConfirmQueue);
			
			JMSConsumer replyConsumer = context.createConsumer(retailerConfirmQueue);

			for (int i = 0; i < NOFORDERS; i++) {
				MapMessage message = context.createMapMessage();
				message.setString("Item", "Computer");
				message.setString("OrderId", UUID.randomUUID().toString());
				message.setInt("Quantity", 1+i);
				producer.send(vendorOrderQueue, message);
				log("Ordered %d computers, ID=%s", 1+i, message.getString("OrderId"));
			}

			for (int i = 0; i < NOFORDERS; i++) {
				Message m = replyConsumer.receive();
				if (m instanceof MapMessage) {
					MapMessage reply = (MapMessage) m;
					String id = reply.getString("OrderId");
					if (reply.getBoolean("OrderAccepted")) {
						log("Order %s Filled", id);
					} else {
						log("Order %s Not Filled", id);
					}
				}
			}			
		} catch (JMSException e) {
			throw new RuntimeException(e);
		}
	}

	private void log(String format, Object... args) {
		int threadId = System.identityHashCode(Thread.currentThread());
		System.out.printf("Retailer: [" + threadId + "] " + format + "\n", args);
	}

	public static void main(String[] args) throws Exception {
		Context jndiContext = new InitialContext();
		ConnectionFactory factory = (ConnectionFactory) jndiContext.lookup("ConnectionFactory");
		Retailer r = new Retailer("VendorOrderQueue", factory);
		new Thread(r, "Retailer").start();
	}

}