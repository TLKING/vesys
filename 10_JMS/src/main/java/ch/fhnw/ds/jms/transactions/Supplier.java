package ch.fhnw.ds.jms.transactions;

import java.util.Random;

import javax.jms.ConnectionFactory;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.Queue;
import javax.naming.Context;
import javax.naming.InitialContext;

/**
 * The Supplier synchronously receives the order from the Vendor and
 * randomly responds with either the number ordered, or some lower
 * quantity. 
 */
public class Supplier implements Runnable {
	private final ConnectionFactory factory;
	private final String item;
	private final String queue;
	
	public Supplier(String item, String queue, ConnectionFactory factory) {
		this.item = item;
		this.queue = queue;
		this.factory = factory;
	}

	public void run() {
		try (JMSContext context = factory.createContext(JMSContext.SESSION_TRANSACTED)) {
			Queue orderQueue = context.createQueue(queue);
			JMSConsumer consumer = context.createConsumer(orderQueue);
			JMSProducer producer = context.createProducer();
			
			while (true) {
				Message message = consumer.receive();
				MapMessage orderMessage;
				if (message instanceof MapMessage) {
					orderMessage = (MapMessage) message;
				
					int quantity = orderMessage.getInt("Quantity");
					log("Vendor ordered %d %s", quantity, orderMessage.getString("Item"));
					
					MapMessage outMessage = context.createMapMessage();
					outMessage.setInt("VendorOrderNumber", orderMessage.getInt("VendorOrderNumber"));
					outMessage.setString("Item", item);
					
					// delivers the requested amount of items or less, depending on stock
					quantity = Math.min(
							orderMessage.getInt("Quantity"),
							new Random().nextInt(orderMessage.getInt("Quantity") * 5)
					);
					outMessage.setInt("Quantity", quantity);
					
					producer.send(message.getJMSReplyTo(), outMessage);
					log("Sent %d %s(s)", quantity, item);
//					if(System.currentTimeMillis() % 2 == 0) {
//						context.commit();
//						log("committed transaction");
//					} else{
//						context.rollback();
//						log("rollbacked transaction");
//					}
					context.commit();
				}
			}
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}
	
	private void log(String format, Object... args) {
		System.out.printf(item + " Supplier: " + format + "\n", args);
	}
	
	public static void main(String[] args) throws Exception {
		Context jndiContext = new InitialContext();
		ConnectionFactory factory = (ConnectionFactory) jndiContext.lookup("ConnectionFactory");

		String item = args[0];
		String queue;
		if ("HardDrive".equals(item)) {
			queue = "StorageOrderQueue";
		} else if ("Monitor".equals(item)) {
			queue = "MonitorOrderQueue";
		} else {
			throw new IllegalArgumentException("Item must be either HardDrive or Monitor");
		}
		
		Supplier s = new Supplier(item, queue, factory);
		new Thread(s, "Supplier " + item).start();
		System.out.println(item + " Supplier started");
	}
}