package ch.fhnw.ds.jms.transactions;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TemporaryQueue;
import javax.naming.Context;
import javax.naming.InitialContext;

/**
 * The Vendor synchronously, and in a single transaction, receives the
 * order from VendorOrderQueue and sends messages to the two Suppliers via
 * MonitorOrderQueue and StorageOrderQueue.
 * The responses are received asynchronously; when both responses come
 * back, the order confirmation message is sent back to the Retailer.
 */
public class Vendor implements Runnable, MessageListener {
	private final ConnectionFactory factory;
	private final String vendorOrderQueueName;
	private final String monitorOrderQueueName;
	private final String storageOrderQueueName;
	
	public Vendor(String vendorOrderQueueName, String monitorOrderQueueName, String storageOrderQueueName, ConnectionFactory factory) {
		this.vendorOrderQueueName = vendorOrderQueueName;
		this.monitorOrderQueueName = monitorOrderQueueName;
		this.storageOrderQueueName = storageOrderQueueName;
		this.factory = factory;
	}

	private	JMSContext asyncSession;
	private Destination monitorReorder;
	private Destination storageReorder;
	private TemporaryQueue vendorConfirmQueue;
	
	public void run() {
		try (JMSContext context = factory.createContext(JMSContext.SESSION_TRANSACTED)) {

			Destination orderQueue = context.createQueue(vendorOrderQueueName);
			Destination monitorOrderQueue = context.createQueue(monitorOrderQueueName);
			Destination storageOrderQueue = context.createQueue(storageOrderQueueName);
			
			JMSConsumer orderConsumer = context.createConsumer(orderQueue);
			JMSProducer producer = context.createProducer();
			
			asyncSession = factory.createContext(JMSContext.SESSION_TRANSACTED);
			monitorReorder = asyncSession.createQueue(monitorOrderQueueName);
			storageReorder = asyncSession.createQueue(storageOrderQueueName);
			vendorConfirmQueue = asyncSession.createTemporaryQueue();
			
			JMSConsumer confirmConsumer = asyncSession.createConsumer(vendorConfirmQueue);
			confirmConsumer.setMessageListener(this);
			
			while (true) {
				try {
					Message inMessage = orderConsumer.receive();
					MapMessage message;
					if (inMessage instanceof MapMessage) {
						message = (MapMessage) inMessage;
					
						Order order = new Order(message);
						
						MapMessage orderMessage = context.createMapMessage();
						orderMessage.setJMSReplyTo(vendorConfirmQueue);
						orderMessage.setInt("VendorOrderNumber", order.getOrderNumber());
						int quantity = message.getInt("Quantity");
						String id = message.getString("OrderId");
						log("Retailer ordered %d %s in Order %s", quantity, message.getString("Item"), id);
						
						orderMessage.setInt("Quantity", quantity);
						orderMessage.setString("Item", "Monitor");
						producer.send(monitorOrderQueue, orderMessage);
						log("ordered %d Monitor(s)", quantity);
						
						orderMessage.setString("Item", "HardDrive");
						producer.send(storageOrderQueue, orderMessage);
						log("ordered %d Hard Drive(s)", quantity);
	
						// Randomly throw an exception in here to simulate a Database error
						// and trigger a rollback of the transaction
						if (new Random().nextInt(3) == 0) {
							order.remove();
							throw new JMSException("Simulated Database Error.");
						}
						context.commit();
						log("Committed Transaction 1");
					}
				} catch (JMSException e) {
					log("JMSException Occured: " + e.getMessage());
					e.printStackTrace();
					context.rollback();
					log("Rolled Back Transaction.");
				}
			}
		}
	}

	// handle messages from the suppliers
	public void onMessage(Message message) {
		try {
			MapMessage componentMessage = (MapMessage) message;
			
			int orderNumber = componentMessage.getInt("VendorOrderNumber");
			Order order = Order.getOrder(orderNumber);
			order.processSubOrder(componentMessage, asyncSession, monitorReorder, storageReorder, vendorConfirmQueue);
			asyncSession.commit();
			
			if (! "Pending".equals(order.getStatus())) {
				log("Completed processing for order %d of retailer order %s", orderNumber, order.getRetailerOrderId());
				
				MapMessage replyMessage = asyncSession.createMapMessage();
				replyMessage.setString("OrderId", order.getMessage().getString("OrderId"));
				if ("Fulfilled".equals(order.getStatus())) {
					replyMessage.setBoolean("OrderAccepted", true);
					log("sent %d computer", order.quantity);
				} else {
					replyMessage.setBoolean("OrderAccepted", false);
					log("unable to send %d computer", order.quantity);
				}
				JMSProducer replyProducer = asyncSession.createProducer();
				replyProducer.send(order.getMessage().getJMSReplyTo(), replyMessage);
				asyncSession.commit();
				log("committed transaction 2");
			}
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}
	
	private static void log(String format, Object... args) {
		int threadId = System.identityHashCode(Thread.currentThread());
		System.out.printf("Vendor: ["+threadId+"] " + format + "\n", args);
	}

	public static class Order {
		private static Map<Integer, Order> pendingOrders = new HashMap<Integer, Order>();
		private static int nextOrderNumber = 1;

		private int orderNumber;
		private int quantity;
		private String retailerOrderId;
		private int monitorPending;
		private int storagePending;
		private MapMessage message;
		private String status;
		
		public Order(MapMessage message) {
			this.orderNumber = nextOrderNumber++;
			this.message = message;
			try {
				this.quantity = message.getInt("Quantity");
				this.retailerOrderId = message.getString("OrderId");
			} catch (JMSException e) {
				throw new RuntimeException(e);
			}
			monitorPending = quantity;
			storagePending = quantity;
			status = "Pending";
			pendingOrders.put(orderNumber, this);
		}
		
		public void remove() { pendingOrders.remove(orderNumber); }
		
		public Object getStatus() { return status; }
		public MapMessage getMessage() { return message; }
		public int getOrderNumber() { return orderNumber; }
		public String getRetailerOrderId() { return retailerOrderId; }
		
		public static Order getOrder(int number) {
			return pendingOrders.get(number);
		}
		
		public void processSubOrder(MapMessage message, JMSContext asyncSession, Destination monitorReorder, Destination storageReorder, TemporaryQueue vendorConfirmQueue) {
			try {
				String itemName = message.getString("Item");
				int n = message.getInt("Quantity");
			
				if ("Monitor".equals(itemName)) {
					monitorPending -= n;
					if(monitorPending > 0) {
						JMSProducer producer = asyncSession.createProducer();
						MapMessage orderMessage = asyncSession.createMapMessage();
						orderMessage.setJMSReplyTo(vendorConfirmQueue);
						orderMessage.setInt("VendorOrderNumber", getOrderNumber());
						orderMessage.setInt("Quantity", monitorPending);
						orderMessage.setString("Item", "Monitor");
						producer.send(monitorReorder, orderMessage);
						log("re-ordered %d Monitor(s)", monitorPending);
					}
				} else if ("HardDrive".equals(itemName)) {
					storagePending -= n;
					if(storagePending > 0) {
						JMSProducer producer = asyncSession.createProducer();
						MapMessage orderMessage = asyncSession.createMapMessage();
						orderMessage.setJMSReplyTo(vendorConfirmQueue);
						orderMessage.setInt("VendorOrderNumber", getOrderNumber());
						orderMessage.setInt("Quantity", storagePending);
						orderMessage.setString("Item", "HardDrive");
						producer.send(storageReorder, orderMessage);
						log("re-ordered %d Hard Drive(s)", storagePending);
					}
				}
				
				if (monitorPending == 0 && storagePending == 0) {
					status = "Fulfilled";
				}
			} catch (JMSException e) {
				status = "Cancelled";
			}
		}		
	}

	public static void main(String[] args) throws Exception {
		Context jndiContext = new InitialContext();
		ConnectionFactory factory = (ConnectionFactory) jndiContext.lookup("ConnectionFactory");

		Vendor v = new Vendor("VendorOrderQueue", "MonitorOrderQueue", "StorageOrderQueue", factory);
		new Thread(v, "Vendor").start();
	}	
}