This demo requires three queues:
- VendorOrderQueue
- MonitorOrderQueue
- StorageOrderQueue
These queues are created at run-time.

You can either start the Demo program (which creates a retailer, a vendor and two suppliers, one for hard drives and the other for monitors).

Alternatively you can also start 4 java programs
- java Retailer
- java Vendor
- java Supplier HardDriver
- java Supplier Monitor


The Retailer places 5 orders in the order queue.
The Vendor orders monitors and hard drives at the suppliers, but if he crashes, then the transaction is rolled back,
i.e. the placed orders are withdrawn and the order remains uncomsumed.


