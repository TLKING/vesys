package bank.http;

import bank.InactiveException;
import bank.OverdrawException;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import static bank.server.Server.*;

/**
 * Created by Pascal on 18.03.2017.
 */

public class Driver implements bank.BankDriver {
    private HttpURLConnection connection;
    private Bank bank = null;
    private String argUrl;

    @Override
    public void connect(String[] args) throws IOException {
        argUrl = args[0];
        bank = new Bank();
    }

    private void setConnection() {
        URL url;
        try {
            url = new URL(argUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setConnectTimeout(4444444);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void disconnect() {
        connection.disconnect();

    }

    @Override
    public bank.http.Driver.Bank getBank() {
        return bank;
    }

    class Bank implements bank.Bank {

        @Override
        public Set<String> getAccountNumbers() throws IOException {
            setConnection();
            ObjectOutputStream out = new ObjectOutputStream(connection.getOutputStream());
            out.writeObject(new String[]{GET_ACCOUNT_NUMBERS});
            out.flush();
            ObjectInputStream in = new ObjectInputStream(connection.getInputStream());
            String[] answer;
            HashSet<String> accountNumbers = new HashSet<>();
            try {
                answer = (String[]) in.readObject();

                if (answer[0].equals(GET_ACCOUNT_NUMBERS)) {
                    for (int i = 1; i < answer.length; i++) {
                        accountNumbers.add(answer[i]);
                    }

                } else if (answer[0].equals("-1")) {
                    throw new IOException(answer[1]);

                }
            } catch (IOException e) {
                throw e;
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            disconnect();
            return accountNumbers;
        }

        @Override
        public String createAccount(String owner) throws IOException {
            setConnection();
            String newAccNo = "";
            try {
                ObjectOutputStream out = new ObjectOutputStream(connection.getOutputStream());
                out.writeObject(new String[]{CREATE_ACCOUNT, owner});
                out.flush();
                ObjectInputStream in = new ObjectInputStream(connection.getInputStream());
                String[] answer = (String[]) in.readObject();

                if (answer[0].equals(CREATE_ACCOUNT)) {
                    newAccNo = answer[1];

                } else if (answer[0].equals(IO_EXCEPTION)) {
                    throw new IOException(answer[1]);


                } else if (answer[0].equals(NUMBER_FORMAT_EXCEPTION)) {
                    throw new NumberFormatException(answer[1]);

                }
            } catch (IOException e) {
                throw e;
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            disconnect();
            return newAccNo;
        }

        @Override
        public boolean closeAccount(String number) throws IOException {
            setConnection();
            Boolean accClosed = false;
            try {
                ObjectOutputStream out = new ObjectOutputStream(connection.getOutputStream());
                out.writeObject(new String[]{CLOSE_ACCOUNT, number});
                out.flush();
                ObjectInputStream in = new ObjectInputStream(connection.getInputStream());
                String[] answer = (String[]) in.readObject();

                if (answer[0].equals(CLOSE_ACCOUNT)) {
                    accClosed = Boolean.parseBoolean(answer[1]);

                } else if (answer[0].equals(IO_EXCEPTION)) {
                    throw new IOException(answer[1]);

                }
            } catch (IOException e) {
                throw e;
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            disconnect();
            return accClosed;
        }

        @Override
        public bank.http.Driver.Account getAccount(String number) throws IOException {
            bank.http.Driver.Account thisAccount = null;
            setConnection();
            try {
                ObjectOutputStream out = new ObjectOutputStream(connection.getOutputStream());
                out.writeObject(new String[]{GET_ACCOUNT, number});
                out.flush();
                ObjectInputStream in = new ObjectInputStream(connection.getInputStream());
                String[] answer = (String[]) in.readObject();
                if (answer[0].equals(GET_ACCOUNT)) {
                    if (!answer[1].equals("null")) {
                        thisAccount = new bank.http.Driver.Account(answer[2], answer[1]);
                        thisAccount.balance = Double.parseDouble(answer[3]);
                        thisAccount.active = Boolean.parseBoolean(answer[4]);
                    }

                } else if (answer[0].equals(IO_EXCEPTION)) {
                    throw new IOException(answer[1]);


                }
            } catch (IOException e) {
                throw e;
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            disconnect();
            return thisAccount;
        }

        @Override
        public void transfer(bank.Account from, bank.Account to, double amount)
                throws IOException, InactiveException, OverdrawException, IllegalArgumentException {
            bank.http.Driver.Account fromAcc = (bank.http.Driver.Account) from;
            bank.http.Driver.Account toAcc = (bank.http.Driver.Account) to;
            setConnection();
            ObjectOutputStream out = new ObjectOutputStream(connection.getOutputStream());
            out.writeObject(new String[]{TRANSFER, fromAcc.number, toAcc.number, String.valueOf(amount)});
            out.flush();
            String[] answer = new String[0];

            try {
                ObjectInputStream in = new ObjectInputStream(connection.getInputStream());
                answer = (String[]) in.readObject();
                if (answer[0].equals(TRANSFER)) {
                    from.setBalance(Double.parseDouble(answer[1]));
                    to.setBalance(Double.parseDouble(answer[2]));
                } else if (answer[0].equals(IO_EXCEPTION)) {
                    throw new IOException(answer[1]);

                } else if (answer[0].equals(OVERDRAW_EXCEPTION)) {
                    throw new OverdrawException(answer[1]);

                } else if (answer[0].equals(INACTIVE_EXCEPTION)) {
                    throw new InactiveException(answer[1]);

                } else if (answer[0].equals(NUMBER_FORMAT_EXCEPTION)) {
                    throw new NumberFormatException(answer[1]);

                } else if (answer[0].equals(ILLEGAL_ARGUMENT_EXCEPTION)) {
                    throw new IllegalArgumentException(answer[1]);
                }

            } catch (IOException | OverdrawException | InactiveException | IllegalArgumentException e) {
                throw e;
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            disconnect();
        }

    }

    class Account implements bank.Account {
        private String number;
        private String owner;
        private double balance;
        private boolean active = true;

        Account(String owner, String accNo) {
            this.owner = owner;
            this.number = accNo;
        }

        @Override
        public double getBalance() throws IOException {
            bank.http.Driver.Account refreshAccount = bank.getAccount(this.number);
            return refreshAccount.balance;
        }

        @Override
        public void setBalance(double balance) {
            this.balance = balance;
        }

        @Override
        public String getOwner() {
            return owner;
        }

        @Override
        public String getNumber() {
            return number;
        }

        @Override
        public boolean isActive() {
            return active;
        }

        @Override
        public void setToggleActivation() throws IOException {

        }

        @Override
        public void deposit(double amount) throws IOException,
                IllegalArgumentException, InactiveException {
            try {
                // Test implementation
                setConnection();
                ObjectOutputStream out = new ObjectOutputStream(connection.getOutputStream());
                out.writeObject(new String[]{DEPOSIT, this.number, String.valueOf(amount)});
                out.flush();
                ObjectInputStream in = new ObjectInputStream(connection.getInputStream());
                String[] answer = (String[]) in.readObject();
                if (answer[0].equals(DEPOSIT)) {
                    this.balance = Double.parseDouble(answer[1]);
                } else if (answer[0].equals(IO_EXCEPTION)) {
                    throw new IOException(answer[1]);
                } else if (answer[0].equals(INACTIVE_EXCEPTION)) {
                    throw new InactiveException(answer[1]);
                } else if (answer[0].equals(ILLEGAL_ARGUMENT_EXCEPTION)) {
                    throw new IllegalArgumentException(answer[1]);
                }
            } catch (IOException | InactiveException | IllegalArgumentException e) {
                throw e;
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            disconnect();
        }

        @Override
        public void withdraw(double amount) throws IOException,
                IllegalArgumentException, OverdrawException, InactiveException {
            try {
                setConnection();
                ObjectOutputStream out = new ObjectOutputStream(connection.getOutputStream());
                out.writeObject(new String[]{WITHDRAW, this.number, String.valueOf(amount)});
                out.flush();
                ObjectInputStream in = new ObjectInputStream(connection.getInputStream());
                String[] answer = (String[]) in.readObject();

                if (answer[0].equals(WITHDRAW)) {
                    this.balance = Double.parseDouble(answer[1]);
                } else if (answer[0].equals(IO_EXCEPTION)) {
                    throw new IOException(answer[1]);

                } else if (answer[0].equals(OVERDRAW_EXCEPTION)) {
                    throw new OverdrawException(answer[1]);

                } else if (answer[0].equals(INACTIVE_EXCEPTION)) {
                    throw new InactiveException(answer[1]);

                } else if (answer[0].equals(NUMBER_FORMAT_EXCEPTION)) {
                    throw new NumberFormatException(answer[1]);
                } else if (answer[0].equals(ILLEGAL_ARGUMENT_EXCEPTION)) {
                    throw new IllegalArgumentException(answer[1]);
                }
            } catch (IOException | OverdrawException | InactiveException | IllegalArgumentException e) {
                throw e;
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            disconnect();
        }
    }
}
