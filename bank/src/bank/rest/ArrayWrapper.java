package bank.rest;

/**
 * Created by Pascal on 19.04.2017.
 */

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ArrayWrapper {
    public String[] myArray = new String[]{};
}

