package bank.rmi;

import java.io.IOException;
import java.rmi.Naming;
import java.rmi.NotBoundException;

/**
 * Created by Pascal on 10.07.2017.
 */
public class Driver implements bank.BankDriver {
    private bank.Bank bank;
    public void connect(String[] args) throws IOException {
        String host = args[0];
        try {
            bank = (bank.Bank) Naming.lookup("rmi://"+host+"/Bank");
        }
        catch(NotBoundException e){
            throw new IOException(e);
        }
    }
    public void disconnect() {
        bank = null;
    }
    public bank.Bank getBank() {
        return bank;
    }
}
