package bank.rmi;

import bank.BankDriver2;
import bank.server.rmi.bankdriver2.RmiBank;

import java.io.IOException;
import java.rmi.Naming;
import java.rmi.NotBoundException;


/**
 * Created by Pascal on 10.07.2017.
 */
public class Driver2 {
    private RmiBank bank;

    public void connect(String[] args) throws IOException {
        try {
            bank = (RmiBank) Naming.lookup(
                    "rmi://" + args[0] + "/" + RmiBank.NAME);
        } catch (NotBoundException e) {
            throw new IOException(e);
        }
    }

    public void disconnect() {
        bank = null;
    }

    public bank.Bank getBank() {
        return bank;
    }

    public void registerUpdateHandler(BankDriver2.UpdateHandler handler)
            throws IOException {
        bank.registerUpdateHandler(new Handler(handler));
    }
}