package bank.rmi;

import bank.BankDriver2;
import bank.server.rmi.bankdriver2.RmiBank;

import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by Pascal on 10.07.2017.
 */
public class Handler extends UnicastRemoteObject
        implements RmiBank.RmiUpdateHandler {
    private BankDriver2.UpdateHandler handler;

    public Handler(BankDriver2.UpdateHandler handler)
            throws RemoteException {
        super();
        this.handler = handler;
    }

    @Override
    public void accountChanged(String id) throws IOException {
        handler.accountChanged(id);
    }
}
