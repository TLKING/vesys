package bank.server;

import java.io.IOException;

import bank.InactiveException;
import bank.OverdrawException;

public class Account implements bank.Account {
    private String number;
    private String owner;
    private double balance;
    private boolean active = true;

    Account(String owner, String accNo) {
        this.owner = owner;
        this.number = accNo;
    }

    @Override
    public double getBalance() {
        return balance;
    }

    @Override
    public String getOwner() {
        return owner;
    }

    @Override
    public String getNumber() {
        return number;
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public void deposit(double amount) throws InactiveException {
        if (!active)
            throw new InactiveException();
        if (amount < 0) {
            throw new IllegalArgumentException();
        } else {
            balance += amount;
        }
    }

    @Override
    public void withdraw(double amount) throws InactiveException, OverdrawException {
        if (!active)
            throw new InactiveException();
        if (amount < 0) {
            throw new IllegalArgumentException();
        }
        if (balance < amount)
            throw new OverdrawException();
        else
            balance -= amount;
    }

    @Override
    public void setBalance(double balance) throws IOException {
    }

    @Override
    public void setToggleActivation() throws IOException {
        this.active = !this.active;
    }

}