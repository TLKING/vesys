package bank.server;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import bank.InactiveException;
import bank.OverdrawException;

public class Bank implements bank.Bank {
		private final Map<String, Account> accounts = new HashMap<>();

		@Override
		public Set<String> getAccountNumbers() {
			Set<String> accNos = new HashSet<>();

			for (Map.Entry<String, Account> account : accounts.entrySet()) {
				if (account.getValue().isActive())
					accNos.add(account.getKey());
			}

			return accNos;
		}

		@Override
		public String createAccount(String owner) {
			String newAccNo = accounts.size() + "";

			accounts.put(newAccNo, new Account(owner, newAccNo));

			return newAccNo;
		}

		@Override
		public boolean closeAccount(String number) throws IOException {
			Account acc = accounts.get(number);

			if (!acc.isActive())
				return false;

			if (acc.getBalance() != 0)
				return false;

			acc.setToggleActivation();

			return true;
		}

		@Override
		public bank.Account getAccount(String number) {
			return accounts.get(number);
		}

		@Override
		public void transfer(bank.Account from, bank.Account to, double amount)
				throws IOException, InactiveException, OverdrawException {
			if (amount < 0)
				throw new IllegalArgumentException();

			if (from.isActive() && to.isActive()) {
				from.withdraw(amount);
				to.deposit(amount);
			} else {
				throw new InactiveException();
			}
		}

	}