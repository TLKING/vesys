package bank.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import bank.server.commands.BankCommand;
import bank.server.commands.CloseAccount;
import bank.server.commands.CreateAccount;
import bank.server.commands.Deposit;
import bank.server.commands.GetAccount;
import bank.server.commands.GetAccountNumbers;
import bank.server.commands.IsActive;
import bank.server.commands.ServerError;
import bank.server.commands.Transfer;
import bank.server.commands.Withdraw;

public class Server {
	public static final String GET_ACCOUNT_NUMBERS = "1";
	public static final String CREATE_ACCOUNT = "2";
	public static final String CLOSE_ACCOUNT = "3";
	public static final String GET_ACCOUNT = "4";
	public static final String TRANSFER = "5";
	public static final String DEPOSIT = "6";
	public static final String WITHDRAW = "7";
	public static final String IS_ACTIVE = "8";
	public static final String IO_EXCEPTION = "-1";
	public static final String OVERDRAW_EXCEPTION = "-2";
	public static final String INACTIVE_EXCEPTION = "-3";
	public static final String NUMBER_FORMAT_EXCEPTION = "-4";
	public static final String ILLEGAL_ARGUMENT_EXCEPTION = "-5";
	public static final String SERVER_ERROR = "0";
	public static final Bank BANK = new Bank();
	
	public static final void run(ObjectInputStream ois, ObjectOutputStream oos) throws ClassNotFoundException, IOException {
		String[] req = (String[]) ois.readObject();
		BankCommand cmd = null;

		while (req != null) {
			for (String st : req)
				System.out.print(st + " ");

			System.out.println();

			switch (req[0]) {
			case Server.GET_ACCOUNT_NUMBERS: {
				cmd = new GetAccountNumbers();
				break;
			}
			case Server.CREATE_ACCOUNT:
				cmd = new CreateAccount();
				break;
			case Server.CLOSE_ACCOUNT:
				cmd = new CloseAccount();
				break;
			case Server.GET_ACCOUNT: {
				cmd = new GetAccount();
				break;
			}
			case Server.TRANSFER:
				cmd = new Transfer();
				break;
			case Server.DEPOSIT:
				cmd = new Deposit();
				break;
			case Server.WITHDRAW:
				cmd = new Withdraw();
				break;
			case Server.IS_ACTIVE:
				cmd = new IsActive();
				break;
			default:
				cmd = new ServerError();
				break;
			}
			oos.writeObject(cmd.execute(req));
			
			oos.flush();
			req = (String[]) ois.readObject();
		}
	}
}
