package bank.server;

import javax.xml.ws.Endpoint;

import bank.server.soap.BankServiceImpl;

public class SoapServer {

	public static void main(String[] args) {
		String url = "http://127.0.0.1:9876/hs";
		Endpoint.publish(url, new BankServiceImpl());
		System.out.println("service published");
		System.out.println("WSDL available at " + url + "?wsdl");
	}

}
