package bank.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SockedServer {
	public static void main(String args[]) throws IOException {
		int port = 1234;
		try (ServerSocket server = new ServerSocket(port)) {
			System.out.println("Startet Echo Server on port " + port);
			ExecutorService pool = Executors.newCachedThreadPool();
			while (true) {
				Socket s = server.accept();
				pool.execute(new Task(s));
			}
		}
	}

	private static class Task implements Runnable {
		private final Socket s;

		Task(Socket socket) {
			this.s = socket;
		}

		public void run() {
			try (ObjectInputStream ois = new ObjectInputStream(s.getInputStream());
					ObjectOutputStream oos = new ObjectOutputStream(s.getOutputStream())) {
				
				Server.run(ois, oos);
				
				s.close();
			} catch (IOException | ClassNotFoundException e) {
				System.err.println(e);
			}
		}
	}
}