package bank.server;

import bank.server.rmi.bankdriver2.BankImpl;
import org.glassfish.tyrus.server.Server;

import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Pascal on 10.07.2017.
 */
@ServerEndpoint(value ="/ws")
public class WsServer {
    private static Bank bank;
    private static List<Session> sessions = new CopyOnWriteArrayList<>();

    @OnOpen
    public void onOpen(Session session) {
        sessions.add(session);
    }

    @OnClose
    public void onClose(Session session, CloseReason closeReason) {
        sessions.remove(session);
    }

    public static void main(String[] args) throws Exception {
        //NOT COMPILING, DON'T KNOW WHY
        /*bank = new BankImpl(new bank.server.Bank(),(String id) -> notifyListeners(id));
        org.glassfish.tyrus.server.Server server = new Server("localhost", 2222, "/bank", null,
                WsServer.class);
        server.start();
        System.out.println("Server started, key to stop the server");
        System.in.read();*/
    }

    private static void notifyListeners(String id) {
        for (Session s : sessions) {
            try {
                s.getBasicRemote().sendText(id);
            } catch (Exception e) {
                sessions.remove(s);
            }
        }
    }


}
