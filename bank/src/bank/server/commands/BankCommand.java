package bank.server.commands;

public interface BankCommand {
	public String[] execute(String[] req);
}
