package bank.server.commands;

import java.io.IOException;

import bank.server.Server;

public class CloseAccount implements BankCommand {

	@Override
	public String[] execute(String[] req) {
		if (req.length < 2)
			return new String[] { Server.SERVER_ERROR, "Not enough Arguments" };
		else {
			try {
				return new String[] { Server.CLOSE_ACCOUNT, Server.BANK.closeAccount(req[1]) ? "True" : "False" };
			} catch (IOException e) {
				return new String[] { Server.IO_EXCEPTION, e.getMessage() };
			}
		}
	}

}
