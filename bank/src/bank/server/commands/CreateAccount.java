package bank.server.commands;

import bank.server.Server;

public class CreateAccount implements BankCommand {

	@Override
	public String[] execute(String[] req) {
		if (req.length < 2)
			return new String[] { Server.SERVER_ERROR, "Not enough Arguments" };
		else
			return new String[] { Server.CREATE_ACCOUNT, Server.BANK.createAccount(req[1]) };
	}

}
