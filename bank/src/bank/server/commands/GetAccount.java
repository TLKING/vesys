package bank.server.commands;

import bank.server.Account;
import bank.server.Server;

public class GetAccount implements BankCommand {

	@Override
	public String[] execute(String[] req) {
		if (req.length < 2)
			return new String[] { Server.SERVER_ERROR, "Not enough Arguments" };
		else {
			Account acc = (Account) Server.BANK.getAccount(req[1]);
			String[] resp = new String[6];
			resp[0] = Server.GET_ACCOUNT;
			if (acc != null) {
				resp[1] = acc.getNumber();
				resp[2] = acc.getOwner();
				resp[3] = acc.getBalance() + "";
				resp[4] = acc.isActive() ? "True" : "False";
			}
			else
			{
				resp[1] = "null";
				resp[2] = "null";
				resp[3] = "null";
				resp[4] = "null";
			}
			return resp;
		}
	}

}
