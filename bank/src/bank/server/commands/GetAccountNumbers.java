package bank.server.commands;

import java.util.Set;

import bank.server.Server;

public class GetAccountNumbers implements BankCommand {

	@Override
	public String[] execute(String[] req) {
		
		Set<String> accountNumbers = Server.BANK.getAccountNumbers();

		String[] resp = new String[accountNumbers.size() + 1];

		int i = 0;

		resp[i++] = Server.GET_ACCOUNT_NUMBERS;

		for (String st : accountNumbers)
			resp[i++] = st;

		return resp;
	}

}
