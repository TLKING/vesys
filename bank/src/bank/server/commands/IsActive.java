package bank.server.commands;

import bank.server.Account;
import bank.server.Server;

public class IsActive implements BankCommand {

	@Override
	public String[] execute(String[] req) {
		if (req.length < 2)
			return new String[] { Server.SERVER_ERROR, "Not enough Arguments" };
		Account acc = (Account) Server.BANK.getAccount(req[1]);

		if (acc == null)
			return new String[] { Server.SERVER_ERROR, "Account not found" };
		

		return new String[] { Server.IS_ACTIVE, acc.isActive()?"True":"False" };
	}
}
