package bank.server.commands;

import bank.server.Server;

public class ServerError implements BankCommand {

	@Override
	public String[] execute(String[] req) {
		return new String[] { Server.SERVER_ERROR, "Cannot do the requested action" };
	}

}
