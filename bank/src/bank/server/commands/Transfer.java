package bank.server.commands;

import java.io.IOException;

import bank.InactiveException;
import bank.OverdrawException;
import bank.server.Server;

public class Transfer implements BankCommand {

	@Override
	public String[] execute(String[] req) {
		if (req.length < 4)
			return new String[] { Server.SERVER_ERROR, "Not enough Arguments" };
		else {
			try {
				Server.BANK.transfer(Server.BANK.getAccount(req[1]), Server.BANK.getAccount(req[2]),
						Double.parseDouble(req[3]));
				return new String[] { Server.TRANSFER, Server.BANK.getAccount(req[1]).getBalance() + "",
						Server.BANK.getAccount(req[1]).getBalance() + "" };
			} catch (NumberFormatException e) {
				return new String[] { Server.NUMBER_FORMAT_EXCEPTION, e.getMessage() };
			} catch (InactiveException e) {
				return new String[] { Server.INACTIVE_EXCEPTION, e.getMessage() };
			} catch (OverdrawException e) {
				return new String[] { Server.OVERDRAW_EXCEPTION, e.getMessage() };
			} catch (IOException e) {
				return new String[] { Server.IO_EXCEPTION, e.getMessage() };
			} catch (IllegalArgumentException e){
				return new String[] {Server.ILLEGAL_ARGUMENT_EXCEPTION, e.getMessage()};
			}
		}
	}

}
