package bank.server.commands;

import bank.InactiveException;
import bank.OverdrawException;
import bank.server.Account;
import bank.server.Server;

public class Withdraw implements BankCommand {

    @Override
    public String[] execute(String[] req) {
        if (req.length < 3)
            return new String[]{Server.SERVER_ERROR, "Not enough Arguments"};
        else {
            try {
                Account acc = (Account) Server.BANK.getAccount(req[1]);
                if (acc != null) {
                    acc.withdraw(Double.parseDouble(req[2]));
                    return new String[]{Server.WITHDRAW, acc.getBalance() + ""};
                } else {
                    return new String[]{Server.SERVER_ERROR, "Account does not exist"};
                }
            } catch (NumberFormatException e) {
                return new String[]{Server.NUMBER_FORMAT_EXCEPTION, e.getMessage()};
            } catch (InactiveException e) {
                return new String[]{Server.INACTIVE_EXCEPTION, e.getMessage()};
            } catch (OverdrawException e) {
                return new String[]{Server.OVERDRAW_EXCEPTION, e.getMessage()};
            } catch (IllegalArgumentException e) {
                return new String[]{Server.ILLEGAL_ARGUMENT_EXCEPTION, e.getMessage()};
            }

        }
    }

}
