package bank.server.rest;

import bank.rest.ArrayWrapper;
import bank.server.commands.*;

import javax.inject.Singleton;
import javax.ws.rs.*;

import static bank.server.Server.*;

@Singleton
@Path("/")
public class RestServices {
    @DELETE
    @Produces("application/json")
    @Consumes("application/json")
    @Path("/closeAccount")
    public ArrayWrapper closeAccount(@QueryParam("id") String id) {
        String[] req = new String[]{CLOSE_ACCOUNT, id};
        ArrayWrapper arrayWrapper = new ArrayWrapper();
        arrayWrapper.myArray = new CloseAccount().execute(req);
        return arrayWrapper;
    }

    //should be @POST
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("/createAccount")
    public ArrayWrapper createAccount(@QueryParam("owner") String owner) {
        String[] req = new String[]{CREATE_ACCOUNT, owner};
        ArrayWrapper arrayWrapper = new ArrayWrapper();
        arrayWrapper.myArray = new CreateAccount().execute(req);
        return arrayWrapper;
    }

    //should be @PUT
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("/deposit")
    public ArrayWrapper deposit(@QueryParam("id") String id, @QueryParam("amount") String amount) {
        String[] req = new String[]{DEPOSIT, id, amount};
        ArrayWrapper arrayWrapper = new ArrayWrapper();
        arrayWrapper.myArray = new Deposit().execute(req);
        return arrayWrapper;
    }

    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("/getAccount")
    public ArrayWrapper getAccount(@QueryParam("id") String id) {
        String[] req = new String[]{GET_ACCOUNT, id};
        ArrayWrapper arrayWrapper = new ArrayWrapper();
        arrayWrapper.myArray = new GetAccount().execute(req);
        return arrayWrapper;
    }

    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("/getAccountNumbers")
    public ArrayWrapper getAccountNumbers() {
        String[] req = new String[]{GET_ACCOUNT_NUMBERS};
        ArrayWrapper a = new ArrayWrapper();
        a.myArray = new GetAccountNumbers().execute(req);
        return a;
    }


    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("/isActive")
    public ArrayWrapper isActive(@QueryParam("id") String id) {
        String[] req = new String[]{IS_ACTIVE, id};
        ArrayWrapper arrayWrapper = new ArrayWrapper();
        arrayWrapper.myArray = new IsActive().execute(req);
        return arrayWrapper;
    }

    //Should be @PUT
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("/transfer")
    public ArrayWrapper transfer(@QueryParam("fromId") String fromId, @QueryParam("toId") String toId, @QueryParam("amount") String amount) {
        String[] req = new String[]{TRANSFER, fromId, toId, amount};
        ArrayWrapper arrayWrapper = new ArrayWrapper();
        arrayWrapper.myArray = new Transfer().execute(req);
        return arrayWrapper;
    }

    //Should be @PUT
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    @Path("/withdraw")
    public ArrayWrapper withdraw(@QueryParam("id") String id, @QueryParam("amount") String amount) {
        String[] req = new String[]{WITHDRAW, id, amount};
        ArrayWrapper arrayWrapper = new ArrayWrapper();
        arrayWrapper.myArray = new Withdraw().execute(req);
        return arrayWrapper;
    }
}
