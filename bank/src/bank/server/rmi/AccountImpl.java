package bank.server.rmi;

import bank.Account;
import bank.InactiveException;
import bank.OverdrawException;

import java.io.IOException;
import java.rmi.RemoteException;

/**
 * Created by Pascal on 10.07.2017.
 */
public class AccountImpl extends java.rmi.server.UnicastRemoteObject
        implements bank.server.rmi.RmiAccount {
    private final Account acc;
    public AccountImpl(Account a) throws RemoteException {
        this.acc = a;
    }
    public String getNumber() throws IOException {
        return acc.getNumber();
    }
    public String getOwner() throws IOException{
        return acc.getOwner();
    }

    @Override
    public boolean isActive() throws IOException {
        return acc.isActive();
    }

    @Override
    public void setToggleActivation() throws IOException {
        acc.setToggleActivation();
    }

    public void deposit(double amount)
            throws IOException, InactiveException {
        acc.deposit(amount);
    }

    @Override
    public void withdraw(double amount) throws IOException, IllegalArgumentException, OverdrawException, InactiveException {
        acc.withdraw(amount);
    }

    @Override
    public double getBalance() throws IOException {
        return acc.getBalance();
    }

    @Override
    public void setBalance(double balance) throws IOException {
        acc.setBalance(balance);
    }

}
