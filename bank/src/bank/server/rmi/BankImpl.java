package bank.server.rmi;

import bank.Account;
import bank.Bank;
import bank.InactiveException;
import bank.OverdrawException;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Pascal on 10.07.2017.
 */
public class BankImpl extends java.rmi.server.UnicastRemoteObject
        implements bank.server.rmi.RmiBank {
    private final Bank bank;

    public BankImpl(Bank bank) throws RemoteException {
        this.bank = bank;
    }

    public String createAccount(String owner) throws IOException {
        return bank.createAccount(owner);
    }

    public boolean closeAccount(String number) throws IOException {
        return bank.closeAccount(number);
    }

    @Override
    public Set<String> getAccountNumbers() throws IOException {
        // makes result set definitively serializable
        return new HashSet<String>(bank.getAccountNumbers());
    }

    @Override
    public Account getAccount(String number) throws IOException {
        bank.Account a = bank.getAccount(number);
        return a==null ? null : new AccountImpl(a);
    }

    @Override
    public void transfer(Account a, Account b, double amount) throws IOException, IllegalArgumentException, OverdrawException, InactiveException {
        bank.transfer(a, b, amount);
    }
}