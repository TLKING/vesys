package bank.server.rmi;
import java.rmi.Remote;
import bank.Account;
/**
 * Created by Pascal on 10.07.2017.
 */
public interface RmiAccount extends Account, Remote{
}
