package bank.server.rmi;

import bank.Bank;

import java.rmi.Naming;

/**
 * Created by Pascal on 10.07.2017.
 */
public class RmiServer {
    public static void main(String[] args) {
        try{
// use e.g. local (threadsafe) bank
            Bank localBank = new bank.server.Bank();
            BankImpl bank = new BankImpl(localBank);
            Naming.rebind("Bank", bank);
            System.out.println("Server started...");
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}
