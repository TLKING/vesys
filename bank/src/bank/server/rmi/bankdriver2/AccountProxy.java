package bank.server.rmi.bankdriver2;

import bank.InactiveException;
import bank.OverdrawException;

import java.io.IOException;
import java.io.Serializable;

/**
 * Created by Pascal on 10.07.2017.
 */
public class AccountProxy implements Serializable, bank.Account {
    private final RmiAccount impl;
    private final String number;
    private final String owner;

    AccountProxy(RmiAccount impl) throws IOException {
        this.impl = impl;
        number = impl.getNumber(); // could also be defined lazily
        owner = impl.getOwner();}

    public String getNumber() {
        return number;
    }

    public String getOwner() {
        return owner;
    }

    public boolean isActive() throws IOException {
        return impl.isActive();
    }

    @Override
    public void setToggleActivation() throws IOException {
        impl.setToggleActivation();
    }

    public void deposit(double amount) throws IOException, InactiveException{
        impl.deposit(amount);
    }
    public void withdraw(double amount) throws IOException, InactiveException, OverdrawException{
        impl.withdraw(amount);
    }
    public double getBalance() throws IOException{
        return impl.getBalance();
    }

    @Override
    public void setBalance(double balance) throws IOException {
        impl.setBalance(balance);
    }
}
