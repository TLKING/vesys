package bank.server.rmi.bankdriver2;

import bank.Account;
import bank.InactiveException;
import bank.OverdrawException;
import bank.server.Bank;

import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Pascal on 10.07.2017.
 */
public class BankImpl extends UnicastRemoteObject
        implements RmiBank {
    private final Bank bank;

    public BankImpl(Bank bank) throws RemoteException {
        this.bank = bank;
    }

    public String createAccount(String owner) throws IOException {
        String id = bank.createAccount(owner);
        if (id != null) notifyListeners(id);
        return id;
    }

    public boolean closeAccount(String number) throws IOException {
        boolean res = bank.closeAccount(number);
        if (res) notifyListeners(number);
        return res;
    }

    private List<RmiUpdateHandler> listeners = new CopyOnWriteArrayList<>();

    void notifyListeners(String id) throws IOException {
        for (RmiBank.RmiUpdateHandler h : listeners) {
            h.accountChanged(id);
        }
    }

    @Override
    public void registerUpdateHandler(RmiBank.RmiUpdateHandler h) {
        listeners.add(h);
    }

    @Override
    public Set<String> getAccountNumbers() throws IOException {
        // makes result set definitively serializable
        return new HashSet<String>(bank.getAccountNumbers());
    }

    @Override
    public bank.Account getAccount(String number) throws IOException {
        bank.Account a = bank.getAccount(number);
        return a == null ? null : new AccountProxy(new AccountImpl(a));
    }

    @Override
    public void transfer(Account a, Account b, double amount) throws IOException, IllegalArgumentException, OverdrawException, InactiveException {
        double prevBal = a.getBalance();
        bank.transfer(a, b, amount);
        if (prevBal+amount == a.getBalance())
        {
            notifyListeners(a.getNumber());
        }
    }

    public class AccountImpl extends java.rmi.server.UnicastRemoteObject
            implements RmiAccount {
        private final Account acc;

        public AccountImpl(Account a) throws RemoteException {
            this.acc = a;
        }

        public String getNumber() throws IOException {
            return acc.getNumber();
        }

        public String getOwner() throws IOException {
            return acc.getOwner();
        }

        @Override
        public boolean isActive() throws IOException {
            return acc.isActive();
        }

        @Override
        public void setToggleActivation() throws IOException {
            acc.setToggleActivation();
        }

        public void deposit(double amount)
                throws IOException, InactiveException {
            acc.deposit(amount);
            BankImpl.this.notifyListeners(acc.getNumber());
        }

        @Override
        public void withdraw(double amount) throws IOException, IllegalArgumentException, OverdrawException, InactiveException {
            acc.withdraw(amount);
            BankImpl.this.notifyListeners(acc.getNumber());
        }

        @Override
        public double getBalance() throws IOException {
            return acc.getBalance();
        }

        @Override
        public void setBalance(double balance) throws IOException {
            acc.setBalance(balance);
            BankImpl.this.notifyListeners(acc.getNumber());
        }
    }
}
