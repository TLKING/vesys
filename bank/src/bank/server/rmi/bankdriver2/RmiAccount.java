package bank.server.rmi.bankdriver2;

import bank.Account;

import java.rmi.Remote;

/**
 * Created by Pascal on 10.07.2017.
 */
public interface RmiAccount extends Account, Remote{
}
