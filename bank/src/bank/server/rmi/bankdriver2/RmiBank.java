package bank.server.rmi.bankdriver2;

import bank.Bank;

import java.io.IOException;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Pascal on 10.07.2017.
 */
public interface RmiBank extends Bank, Remote {
    public String NAME = RmiBank.class.getName();

    interface RmiUpdateHandler extends Remote {
        void accountChanged(String id) throws IOException;
    }

    public void registerUpdateHandler(RmiUpdateHandler handler) throws RemoteException;
}
