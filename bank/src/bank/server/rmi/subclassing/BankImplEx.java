package bank.server.rmi.subclassing;

import bank.server.rmi.AccountImpl;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Pascal on 10.07.2017.
 */
public class BankImplEx extends bank.server.Bank implements bank.server.rmi.RmiBank {
    public Set<String> getAccountNumbers() {
        return new HashSet<String>(super.getAccountNumbers());
    }

    public bank.Account getAccount(String number) {
        try {
            bank.Account a = super.getAccount(number);
            return a == null ? null : new AccountImpl(a);
        } catch (IOException e) {
            return null;
        }
    }
}
