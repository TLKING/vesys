package bank.server.soap;

import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface BankService {
	String[] closeAccount(@WebParam(name = "req") String[] req);
	String[] createAccount(@WebParam(name = "req") String[] req);
	String[] deposit(@WebParam(name = "req") String[] req);
	String[] getAccount(@WebParam(name = "req") String[] req);
	String[] getAccountNumbers(@WebParam(name = "req") String[] req);
	String[] isActive(@WebParam(name = "req") String[] req);
	String[] transfer(@WebParam(name = "req") String[] req);
	String[] withdraw(@WebParam(name = "req") String[] req);
}
