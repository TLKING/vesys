package bank.server.soap;

import javax.jws.WebParam;
import javax.jws.WebService;

import bank.server.commands.CloseAccount;
import bank.server.commands.CreateAccount;
import bank.server.commands.Deposit;
import bank.server.commands.GetAccount;
import bank.server.commands.GetAccountNumbers;
import bank.server.commands.IsActive;
import bank.server.commands.Transfer;
import bank.server.commands.Withdraw;

@WebService
public class BankServiceImpl implements BankService {

	@Override
	public String[] closeAccount(@WebParam(name = "req") String[] req) {
		return new CloseAccount().execute(req);
	}

	@Override
	public String[] createAccount(@WebParam(name = "req") String[] req) {
		return new CreateAccount().execute(req);
	}

	@Override
	public String[] deposit(@WebParam(name = "req") String[] req) {
		return new Deposit().execute(req);
	}

	@Override
	public String[] getAccount(@WebParam(name = "req") String[] req) {
		return new GetAccount().execute(req);
	}

	@Override
	public String[] getAccountNumbers(@WebParam(name = "req") String[] req) {
		return new GetAccountNumbers().execute(req);
	}

	@Override
	public String[] isActive(@WebParam(name = "req") String[] req) {
		return new IsActive().execute(req);
	}

	@Override
	public String[] transfer(@WebParam(name = "req") String[] req) {
		return new Transfer().execute(req);
	}

	@Override
	public String[] withdraw(@WebParam(name = "req") String[] req) {
		return new Withdraw().execute(req);
	}

}
