package bank.soap;

/**
 * Created by Pascal on 01.04.2017.
 */
/*
 * Copyright (c) 2000-2017 Fachhochschule Nordwestschweiz (FHNW)
 * All Rights Reserved.
 */

import bank.InactiveException;
import bank.OverdrawException;

import java.io.*;
import java.util.*;

import bank.server.soap.BankServiceImpl;
import static bank.server.Server.*;

public class Driver implements bank.BankDriver {
    private Bank bank = null;
    private BankServiceImpl bankService;

    @Override
    public void connect(String[] args) throws IOException {
        bank = new Bank();
        bankService = new BankServiceImpl();
    }

    @Override
    public void disconnect() {
    }

    @Override
    public Bank getBank() {
        return bank;
    }

    class Bank implements bank.Bank {

        @Override
        public Set<String> getAccountNumbers() throws IOException {
            String [] answer = bankService.getAccountNumbers(new String[]{GET_ACCOUNT_NUMBERS});
            HashSet<String> accountNumbers = new HashSet<>();
            try {

                if (answer[0].equals(GET_ACCOUNT_NUMBERS)) {
                    for (int i = 1; i < answer.length; i++) {
                        accountNumbers.add(answer[i]);
                    }

                } else if (answer[0].equals(IO_EXCEPTION)) {
                    throw new IOException(answer[1]);

                }
            } catch (IOException e) {
                throw e;
            }

            return accountNumbers;
        }

        @Override
        public String createAccount(String owner) throws IOException {
            String newAccNo = "";
            try {
                String[] answer = bankService.createAccount(new String[]{CREATE_ACCOUNT,owner});

                if (answer[0].equals(CREATE_ACCOUNT)) {
                    newAccNo = answer[1];

                } else if (answer[0].equals(IO_EXCEPTION)) {
                    throw new IOException(answer[1]);


                } else if (answer[0].equals(NUMBER_FORMAT_EXCEPTION)) {
                    throw new NumberFormatException(answer[1]);

                }
            } catch (IOException  e) {
                throw e;
            }

            return newAccNo;
        }

        @Override
        public boolean closeAccount(String number) throws IOException {
            Boolean accClosed = false;
            try {
                String[] answer = bankService.closeAccount(new String[]{CLOSE_ACCOUNT,number});

                if (answer[0].equals(CLOSE_ACCOUNT)) {
                    accClosed = Boolean.parseBoolean(answer[1]);

                } else if (answer[0].equals(IO_EXCEPTION)) {
                    throw new IOException(answer[1]);

                }
            } catch (IOException e) {
                throw e;
            }

            return accClosed;
        }

        @Override
        public Account getAccount(String number) throws IOException{
            Account thisAccount = null;

            try {
                String[] answer = bankService.getAccount(new String[]{GET_ACCOUNT,number});
                if (answer[0].equals(GET_ACCOUNT)) {
                    if (!answer[1].equals("null")) {
                        thisAccount = new Account(answer[2], answer[1]);
                        thisAccount.balance = Double.parseDouble(answer[3]);
                        thisAccount.active = Boolean.parseBoolean(answer[4]);
                    }

                } else if (answer[0].equals(IO_EXCEPTION)) {
                    throw new IOException(answer[1]);


                }
            } catch (IOException e) {
                throw e;
            }

            return thisAccount;
        }

        @Override
        public void transfer(bank.Account from, bank.Account to, double amount)
                throws IOException, InactiveException, OverdrawException, IllegalArgumentException {
            Account fromAcc = (Account) from;
            Account toAcc = (Account) to;

            try {
                String[] answer = bankService.transfer(new String[]{TRANSFER, fromAcc.number, toAcc.number, String.valueOf(amount)});
                if (answer[0].equals(TRANSFER)) {
                    from.setBalance(Double.parseDouble(answer[1]));
                    to.setBalance(Double.parseDouble(answer[2]));
                } else if (answer[0].equals(IO_EXCEPTION)) {
                    throw new IOException(answer[1]);

                } else if (answer[0].equals(OVERDRAW_EXCEPTION)) {
                    throw new OverdrawException(answer[1]);

                } else if (answer[0].equals(INACTIVE_EXCEPTION)) {
                    throw new InactiveException(answer[1]);

                } else if (answer[0].equals(NUMBER_FORMAT_EXCEPTION)) {
                    throw new NumberFormatException(answer[1]);

                }else if (answer[0].equals(ILLEGAL_ARGUMENT_EXCEPTION)){
                    throw new IllegalArgumentException(answer[1]);
                }

            } catch (IOException | OverdrawException |  InactiveException| IllegalArgumentException e) {
                throw e;
            }


        }

    }

    class Account implements bank.Account {
        private String number;
        private String owner;
        private double balance;
        private boolean active = true;

        Account(String owner, String accNo) {
            this.owner = owner;
            this.number = accNo;
        }

        @Override
        public double getBalance() throws IOException {
            Account refreshAccount = bank.getAccount(this.number);
            return refreshAccount.balance;
        }

        @Override
        public void setBalance(double balance) {
            this.balance = balance;
        }

        @Override
        public String getOwner() {
            return owner;
        }

        @Override
        public String getNumber() {
            return number;
        }

        @Override
        public boolean isActive() {
            return active;
        }

        @Override
        public void deposit(double amount) throws IOException,
                IllegalArgumentException, InactiveException{
            try {
                // Test implementation
                String[] answer = bankService.deposit(new String[]{DEPOSIT,this.number, String.valueOf(amount)});
                if (answer[0].equals(DEPOSIT)) {
                    this.balance = Double.parseDouble(answer[1]);
                } else if (answer[0].equals(IO_EXCEPTION)) {
                    throw new IOException(answer[1]);
                } else if (answer[0].equals(INACTIVE_EXCEPTION)) {
                    throw new InactiveException(answer[1]);
                }
                else if (answer[0].equals(ILLEGAL_ARGUMENT_EXCEPTION)){
                    throw new IllegalArgumentException(answer[1]);
                }
            } catch (IOException | InactiveException | IllegalArgumentException e) {
                throw e;
            }
        }

        @Override
        public void withdraw(double amount) throws IOException,
                IllegalArgumentException, OverdrawException, InactiveException {
            try {
                String[] answer = bankService.withdraw(new String[]{WITHDRAW, this.number, String.valueOf(amount)});

                if (answer[0].equals(WITHDRAW)) {
                    this.balance = Double.parseDouble(answer[1]);
                } else if (answer[0].equals(IO_EXCEPTION)) {
                    throw new IOException(answer[1]);

                } else if (answer[0].equals(OVERDRAW_EXCEPTION)) {
                    throw new OverdrawException(answer[1]);

                } else if (answer[0].equals(INACTIVE_EXCEPTION)) {
                    throw new InactiveException(answer[1]);

                } else if (answer[0].equals(NUMBER_FORMAT_EXCEPTION)) {
                    throw new NumberFormatException(answer[1]);
                }
                else if (answer[0].equals(ILLEGAL_ARGUMENT_EXCEPTION)){
                    throw new IllegalArgumentException(answer[1]);
                }
            } catch (IOException | OverdrawException | InactiveException | IllegalArgumentException e) {
                throw e;
            }
        }

        @Override
        public void setToggleActivation() throws IOException {
        }
    }
}
