package bank.sockets;

/**
 * Created by Pascal on 02.03.2017.
 */
/*
 * Copyright (c) 2000-2017 Fachhochschule Nordwestschweiz (FHNW)
 * All Rights Reserved.
 */

import bank.InactiveException;
import bank.OverdrawException;

import java.io.*;
import java.net.Socket;
import java.rmi.ServerException;
import java.util.*;

public class Driver implements bank.BankDriver {
    private Bank bank = null;
    private Socket connection;
    private ObjectOutputStream out = null;
    private ObjectInputStream in = null;

    @Override
    public void connect(String[] args) throws IOException {
        String server = args[0];
        Integer port = Integer.parseInt(args[1]);
        connection = new Socket(server, port);
        out = new ObjectOutputStream(connection.getOutputStream());
        in = new ObjectInputStream(connection.getInputStream());
        bank = new Bank();
        // bank.getAccountNumbers();
    }

    @Override
    public void disconnect() {
        try {
            connection.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Bank getBank() {
        return bank;
    }

    class Bank implements bank.Bank {

        @Override
        public Set<String> getAccountNumbers() throws IOException {
            out.writeObject(new String[]{"1"});
            String[] answer;
            HashSet<String> accountNumbers = new HashSet<>();
            try {
                answer = (String[]) in.readObject();

                if (answer[0].equals("1")) {
                    for (int i = 1; i < answer.length; i++) {
                        accountNumbers.add(answer[i]);
                    }

                } else if (answer[0].equals("-1")) {
                    throw new IOException(answer[1]);

                }
            } catch (IOException e) {
                throw e;
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            return accountNumbers;
        }

        @Override
        public String createAccount(String owner) throws IOException {
            String newAccNo = "";
            try {
                out.writeObject(new String[]{"2", owner});
                out.flush();
                String[] answer = (String[]) in.readObject();

                if (answer[0].equals("2")) {
                    newAccNo = answer[1];

                } else if (answer[0].equals("-1")) {
                    throw new IOException(answer[1]);


                } else if (answer[0].equals("-4")) {
                    throw new NumberFormatException(answer[1]);

                }
            } catch (IOException  e) {
                throw e;
            }
            catch (ClassNotFoundException e){
                e.printStackTrace();
            }

            return newAccNo;
        }

        @Override
        public boolean closeAccount(String number) throws IOException {
            Boolean accClosed = false;
            try {
                out.writeObject(new String[]{"3", number});
                out.flush();
                String[] answer = (String[]) in.readObject();

                if (answer[0].equals("3")) {
                    accClosed = Boolean.parseBoolean(answer[1]);

                } else if (answer[0].equals("-1")) {
                    throw new IOException(answer[1]);

                }
            } catch (IOException e) {
                throw e;
            }
            catch (ClassNotFoundException e){
                e.printStackTrace();
            }

            return accClosed;
        }

        @Override
        public Account getAccount(String number) throws IOException{
            Account thisAccount = null;

            try {
                out.writeObject(new String[]{"4", number});

                out.flush();
                String[] answer = (String[]) in.readObject();
                if (answer[0].equals("4")) {
                    if (!answer[1].equals("null")) {
                        thisAccount = new Account(answer[2], answer[1]);
                        thisAccount.balance = Double.parseDouble(answer[3]);
                        thisAccount.active = Boolean.parseBoolean(answer[4]);
                    }

                } else if (answer[0].equals("-1")) {
                    throw new IOException(answer[1]);


                }
            } catch (IOException e) {
                throw e;
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            return thisAccount;
        }

        @Override
        public void transfer(bank.Account from, bank.Account to, double amount)
                throws IOException, InactiveException, OverdrawException, IllegalArgumentException {
            Account fromAcc = (Account) from;
            Account toAcc = (Account) to;
            out.writeObject(new String[]{"5", fromAcc.number, toAcc.number, String.valueOf(amount)});
            out.flush();
            String[] answer = new String[0];

            try {
                answer = (String[]) in.readObject();
                if (answer[0].equals("5")) {
                    from.setBalance(Double.parseDouble(answer[1]));
                    to.setBalance(Double.parseDouble(answer[2]));
                } else if (answer[0].equals("-1")) {
                    throw new IOException(answer[1]);

                } else if (answer[0].equals("-2")) {
                    throw new OverdrawException(answer[1]);

                } else if (answer[0].equals("-3")) {
                    throw new InactiveException(answer[1]);

                } else if (answer[0].equals("-4")) {
                    throw new NumberFormatException(answer[1]);

                }else if (answer[0].equals("-5")){
                    throw new IllegalArgumentException(answer[1]);
                }

            } catch (IOException | OverdrawException |  InactiveException| IllegalArgumentException e) {
                throw e;
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }


        }

    }

    class Account implements bank.Account {
        private String number;
        private String owner;
        private double balance;
        private boolean active = true;

        Account(String owner, String accNo) {
            this.owner = owner;
            this.number = accNo;
        }

        @Override
        public double getBalance() throws IOException {
            Account refreshAccount = bank.getAccount(this.number);
            return refreshAccount.balance;
        }

        @Override
        public void setBalance(double balance) {
            this.balance = balance;
        }

        @Override
        public String getOwner() {
            return owner;
        }

        @Override
        public String getNumber() {
            return number;
        }

        @Override
        public boolean isActive() {
            return active;
        }

        @Override
        public void deposit(double amount) throws IOException,
                IllegalArgumentException, InactiveException{
            try {
                // Test implementation
                out.writeObject(new String[]{"6", this.number, String.valueOf(amount)});
                out.flush();
                String[] answer = (String[]) in.readObject();
                if (answer[0].equals("6")) {
                    this.balance = Double.parseDouble(answer[1]);
                } else if (answer[0].equals("-1")) {
                    throw new IOException(answer[1]);
                } else if (answer[0].equals("-3")) {
                    throw new InactiveException(answer[1]);
                }
                else if (answer[0].equals("-5")){
                    throw new IllegalArgumentException(answer[1]);
                }
            } catch (IOException | InactiveException | IllegalArgumentException e) {
                throw e;
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void withdraw(double amount) throws IOException,
                IllegalArgumentException, OverdrawException, InactiveException {
            try {
                out.writeObject(new String[]{"7", this.number, String.valueOf(amount)});
                out.flush();
                String[] answer = (String[]) in.readObject();

                if (answer[0].equals("7")) {
                    this.balance = Double.parseDouble(answer[1]);
                } else if (answer[0].equals("-1")) {
                    throw new IOException(answer[1]);

                } else if (answer[0].equals("-2")) {
                    throw new OverdrawException(answer[1]);

                } else if (answer[0].equals("-3")) {
                    throw new InactiveException(answer[1]);

                } else if (answer[0].equals("-4")) {
                    throw new NumberFormatException(answer[1]);
                }
                else if (answer[0].equals("-5")){
                    throw new IllegalArgumentException(answer[1]);
                }
            } catch (IOException | OverdrawException | InactiveException | IllegalArgumentException e) {
                throw e;
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void setToggleActivation() throws IOException {
        }
    }
}
