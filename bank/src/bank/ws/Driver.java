package bank.ws;

import bank.Bank;

import javax.websocket.DeploymentException;
import javax.websocket.Session;
import org.glassfish.tyrus.core.BaseContainer;
import org.glassfish.tyrus.client.ClientManager;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Pascal on 10.07.2017.
 */
public class Driver implements bank.BankDriver2 {
    private List<UpdateHandler> listeners = new CopyOnWriteArrayList<>();
    private bank.Bank bank;

    private Session session;


    @Override
    public void registerUpdateHandler(UpdateHandler handler) throws IOException {
        listeners.add(handler);
    }

    @Override
    public void connect(String[] args) throws IOException {
        try {
            URI uri= new URI("ws://" + args[0] + "/bank/ws");
            System.out.println("connecting to " + uri);
            ClientManager client = ClientManager.createClient();
            session = client.connectToServer(this, uri);
        } catch (URISyntaxException | DeploymentException e) {
            throw new IOException(e);
        }
    }

    @Override
    public void disconnect() throws IOException {
        bank = null;
        session.close();
        System.out.println("disconnected...");
    }

    @Override
    public Bank getBank() {
        return bank;
    }
}
